<?php

namespace Drupal\webform_bic_field\Tests;

use Drupal\Tests\webform\Functional\Element\WebformElementBrowserTestBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
/**
 * Tests for webform_bic_field.
 *
 * @group Webform
 */
class WebformBicFieldTest extends WebformElementBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['webform_bic_field'];

  /**
   * Tests SWIFT/BIC field.
   */
  public function testWebforBicField() {
    $webform = Webform::load('webform_bic_field');

    // Check form element rendering.
    $this->drupalGet('webform/webform_bic_field');

    $this->assertSession()->fieldExists('webform_bic_field');
    $this->assertSession()->fieldExists('webform_bic_field_multiple[items][0][_item_]');
    $this->assertSession()->elementExists('.form-text.webform-bic-field');

    // Check webform element submission.

    // Submission fail.
    $edit = [
      'webform_bic_field' => '{Test}',
      'webform_bic_field_multiple[items][0][_item_]' => '{Test 01}',
    ];
    $sid = $this->postSubmission($webform, $edit);
    $this->assertEquals($sid, NULL);

    // Submission succeed.
    $edit = [
      'webform_bic_field' => 'SMCOGB2L',
      'webform_bic_field_multiple[items][0][_item_]' => 'SMCOGB2L',
    ];
    $sid = $this->postSubmission($webform, $edit);
    $webform_submission = WebformSubmission::load($sid);
    $this->assertEquals($webform_submission->getElementData('webform_bic_field'), 'SMCOGB2L');
    $this->assertEquals($webform_submission->getElementData('webform_bic_field_multiple'), ['SMCOGB2L']);
  }

}
