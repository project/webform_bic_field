<?php

namespace Drupal\webform_bic_field\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\Constraints\Bic;
use Symfony\Component\Validator\Validation;


/**
 * Provides a 'webform_bic_field'.
 *
 * @FormElement("webform_bic_field")
 */
class WebformBicField extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 11,
      '#process' => [
        [$class, 'processWebformBicField'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateWebformBicField'],
      ],
      '#pre_render' => [
        [$class, 'preRenderWebformBicField'],
      ],
      '#theme' => 'input__webform_bic_field',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes 'webform_bic_field' element.
   *
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $complete_form
   *
   * @return
   */
  public static function processWebformBicField($element, FormStateInterface $form_state, &$complete_form) {
    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_bic_field'.
   *
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $complete_form
   */
  public static function validateWebformBIcField(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add custom validation logic.
    $name = $element['#name'];
    $value = $form_state->getValue($name);
    $valid = TRUE;
    $multiple = $element['#webform_multiple'] ?? FALSE;

    if ($multiple && !$value) {
      $value = $element['#value'];
    }

    if ($value) {
      $validator = Validation::createValidator();
      $violations = $validator->validate($value, [
        new Bic(),
      ]);

      if (0 !== count($violations)) {
        $valid = FALSE;
      }
    }

    if (!$valid) {
      if (isset($element['#title'])) {
        $tArgs = array(
          '%name' => empty($element['#title']) ? $element['#parents'][0] : $element['#title'],
          '%value' => $value,
        );
        $form_state->setError(
          $element,
          t('The value %value for element %name is not a valid SWIFT/BIC.', $tArgs)
        );
      } else {
        $form_state->setError($element);
      }
    }
  }

  /**
   * Prepares a #type 'webform_bic_field_multiple' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderWebformBicField(array $element) {
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, ['id', 'name', 'value', 'size', 'maxlength', 'placeholder']);
    static::setAttributes($element, ['form-text', 'webform-bic-field']);
    return $element;
  }

}
